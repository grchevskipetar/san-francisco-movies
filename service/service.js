'use strict'

var express = require('express');
var request = require('request');
var fs = require('fs');
var elasticSearch = require('elasticsearch');
var config = require('../config/config');
var _ = require('lodash');
var uuid = require('uuid');

var logger = require('../util/logger');
var logManager = new logger(config.logType);


var esClient = new elasticSearch.Client({
    host: 'http://search-sanfranciscomovies-yzfn6afd5bwlpj4v4y2uosniaa.us-west-2.es.amazonaws.com/'
});


function Service() {

}

Service.prototype.prepareData = function (data,alias) {
    console.log("non-async preparation of data");
    var items = [];
    data.forEach(function(element){
        if(element.locations){
            var action = {
                index:{
                    _index: alias,
                    _type: "movie",
                    _id: uuid.v4()
                }
            }

            var actionData = {
                title:element.title,
                location:element.locations,
                writer:element.writer,
                actor_1:element.actor_1,
                actor_2:element.actor_2,
                actor_3:element.actor_3,
                director:element.director,
                production_company: element.production_company,
                release_year: element.release_year,
            };

            if(element.title){
                let keywords = element.title.split(" ");
                keywords.push(element.title);

                actionData.titleSuggest = {
                    input: keywords,
                    output: element.title
                }
            }

            if(element.locations){
                let keywords = element.locations.split(" ");
                keywords.push(element.locations);
                actionData.locationSuggest = {
                    input: keywords,
                    output: element.locations
                }
            }

            if(element.writer){
                let keywords = element.writer.split(" ");
                keywords.push(element.writer);

                actionData.writerSuggest = {
                    input: keywords,
                    output: element.writer
                }
            }

            if(element.actor_1){
                let keywords = element.actor_1.split(" ");
                keywords.push(element.actor_1);

                actionData.actor_1Suggest = {
                    input: keywords,
                    output: element.actor_1
                }
            }
            if(element.actor_2){
                let keywords = element.actor_2.split(" ");
                keywords.push(element.actor_2);

                actionData.actor_2Suggest = {
                    input: keywords,
                    output: element.actor_2
                }
            }

            if(element.actor_3){
                let keywords = element.actor_3.split(" ");
                keywords.push(element.actor_3);

                actionData.actor_3Suggest = {
                    input: keywords,
                    output: element.actor_3
                }
            }

            if(element.director){
                let keywords = element.director.split(" ");
                keywords.push(element.director);

                actionData.directorSuggest = {
                    input: keywords,
                    output: element.director
                }
            }

            if(element.production_company){
                let keywords = element.production_company.split(" ");
                keywords.push(element.production_company);

                actionData.production_companySuggest = {
                    input: keywords,
                    output: element.production_company
                }
            }

            if(element.release_year){
                let keywords = element.release_year.split(" ");
                keywords.push(element.release_year);

                actionData.release_yearSuggest = {
                    input: keywords,
                    output: element.release_year
                }
            }
                

            items.push(action);
            items.push(actionData);
        }
    });
    return items;
};

Service.prototype.prepareDataForIndexing = function(data,alias){
    var self = this;

    console.log("preparing data for indexing");
    return new Promise(function(resolve,reject){
        var items = self.prepareData(data,alias);
        resolve(items);
    });
}

Service.prototype.bulkIndexItems = function(items){
    console.log("bulk indexing items in elasticsearch");
    return esClient.bulk({body:items});
}

Service.prototype.indexData = function(data, alias){
    var self = this;

    console.log("indexing");
    return self.prepareDataForIndexing(data,alias).then(items => {
        return self.bulkIndexItems(items);
    });
}

Service.prototype.init = function(){
    var self = this;

    console.log("initializing");
    return esClient.indices.delete({index:"_all"}).then(_ => {
        return esClient.indices.create({index:config.alias+"_1"}).then(_ => {
            return self.createAlias(config.alias, config.alias+"*").then(_ => {
                return self.initMapping().then(_ => {
                    return self.getDataForImport().then(data => {
                        logManager.log("Performed initialization");
                        return self.indexData(data, config.alias);
                    });
                })
            });
        });
    });
}

Service.prototype.reIndex = function(){
    var self = this;

    console.log("reindexing");
    return esClient.indices.getAlias({name:config.alias}).then(response => {
        var currentIndex = Object.keys(response)[0];
        var indexNumber = parseInt(currentIndex.split("_")[1],10);
        indexNumber++;
        return esClient.indices.create({index:config.alias+"_"+indexNumber}).then(res => {
            return self.createAlias(config.alias, config.alias+"*").then(_ => {
                return esClient.indices.delete({index:currentIndex}).then(_ => {
                    return self.initMapping().then(_ => {
                        return self.getDataForImport().then(data => {
                            logManager.log("Performed reindexing");
                            return self.indexData(data, config.alias);
                        });
                    }) 
                });
            });
        });
    });
}

Service.prototype.getDataForImport = function(alias, index){
    console.log("getting data for import");
    return new Promise(function(resolve,reject){
        request('https://data.sfgov.org/resource/wwmu-gmzc.json', function(err, response, body){
            if(err !== null) return reject(err);
            resolve(JSON.parse(body));
        });
    })
}

Service.prototype.createAlias = function(alias, index){
    console.log("creating alias");
    return esClient.indices.putAlias({name:alias, index:index});
}

//TODO TEST
Service.prototype.initMapping = function(){
    console.log("initializing mapping");
    return esClient.indices.putMapping({
        index:config.alias,
        type:"movie",
        body: {
            properties:{
                title: { type: "string" },
                location: { type: "string" },
                writer: { type: "string" },
                actor_1: { type: "string" },
                actor_2: { type: "string" },
                actor_3: { type: "string" },
                director: { type: "string" },
                production_company:  { type: "string" },
                release_year:  { type: "string" },
                titleSuggest: {
                    type : "completion",
                },
                locationSuggest: {
                    type : "completion",
                },
                writerSuggest: {
                    type : "completion",
                },
                actor_1Suggest: {
                    type : "completion",
                },
                actor_2Suggest: {
                    type : "completion",
                },
                actor_3Suggest: {
                    type : "completion",
                },
                directorSuggest: {
                    type: "completion",
                },
                production_companySuggest: {
                    type: "completion",
                },
                release_yearSuggest:  { 
                    type: "completion" ,
                }
            }
        }
    });
}

//TODO clean-up function
Service.prototype.autocompleteSuggestions = function(inputText){
    var self = this;

    console.log("getting autocomplete suggestions");
    return esClient.suggest({
        index:config.alias,
        type:"movie",
        body: {
            titleSuggest:{
                text:inputText,
                completion: {
                    field: "titleSuggest",
                    fuzzy:true
                }
            },
            locationSuggest:{
                text:inputText,
                completion: {
                    field: "locationSuggest",
                    fuzzy:true
                }
            },
            writerSuggest:{
                text:inputText,
                completion: {
                    field: "writerSuggest",
                    fuzzy:true
                }
            },
            actor_1Suggest:{
                text:inputText,
                completion: {
                    field: "actor_1Suggest",
                    fuzzy:true
                }
            },
            actor_2Suggest:{
                text:inputText,
                completion: {
                    field: "actor_2Suggest",
                    fuzzy:true
                }
            },
            actor_2Suggest:{
                text:inputText,
                completion: {
                    field: "actor_2Suggest",
                    fuzzy:true
                }
            },
            directorSuggest:{
                text:inputText,
                completion: {
                    field: "directorSuggest",
                    fuzzy:true
                }
            },
            production_companySuggest:{
                text:inputText,
                completion: {
                    field: "production_companySuggest",
                    fuzzy:true
                }
            },
            release_yearSuggest:{
                text:inputText,
                completion: {
                    field: "release_yearSuggest",
                    fuzzy:true
                }
            }
        }
    }).then(response => {

        var suggestions = [];
        suggestions = suggestions.concat(self.prepareSuggestions(response,"titleSuggest"));
        suggestions = suggestions.concat(self.prepareSuggestions(response,"locationSuggest"));
        suggestions = suggestions.concat(self.prepareSuggestions(response,"writerSuggest"));
        suggestions = suggestions.concat(self.prepareSuggestions(response,"actor_1Suggest"));
        suggestions = suggestions.concat(self.prepareSuggestions(response,"actor_2Suggest"));
        suggestions = suggestions.concat(self.prepareSuggestions(response,"actor_3Suggest"));
        suggestions = suggestions.concat(self.prepareSuggestions(response,"production_companySuggest"));
        suggestions = suggestions.concat(self.prepareSuggestions(response,"release_yearSuggest"));

        return new Promise(function(resolve,reject){
            logManager.log("Performed autocomplete suggest");
            resolve(suggestions);
        });
    });
}


//TODO TEST
Service.prototype.prepareSuggestions = function(response,type){
    var suggest = response[type];
    if(!suggest){
        return [];
    }
    else{
        return suggest[0].options;
    }
}

Service.prototype.search = function(searchTerm, index){
    console.log("gettin results for search");
    console.log(searchTerm);
    return esClient.search({
      index: index,
      q: searchTerm
    }).then(response => {
        return new Promise(function(resolve,reject){
            if(response.hits){
                logManager.log("Performed search");
                resolve(response.hits);
            } else{
                logManager.log("Performed search that ended with an error", true);
                reject(response);
            }
        });
    });
}

Service.prototype.indexExists = function(index){
    console.log("checking if index exists");
    return esClient.indices.exists({index:index});
}

Service.prototype.status = function(index){
    var self = this;
    console.log("checking status for service");
    var status = {
        service: "",
        search: "",
        autocomplete:""
    }
    return self.search('American Graffiti').then( results => {
        if(!_.isEmpty(results)) status.search = "OK"
        else status.search = "UNHEALTHY"
        return self.autocompleteSuggestions('Americ').then(results => {
            if(!_.isEmpty(results)) status.autocomplete = "OK"
            else status.autocomplete = "UNHEALTHY"
            return new Promise(function(resolve,reject){
                if(status.search == "OK" && status.autocomplete == "OK")
                    status.service = "OK"
                else
                    status.service = "UNHEALTHY"

                logManager.log("Performed status check");
                resolve(status);
            });
        });
    });
}

//TODO TEST
Service.prototype.getMapping = function(index){
    console.log("getting mapping");
    return esClient.indices.getMapping({index:index}).then().then(response => {
        return new Promise(function(resolve,reject){
            if(response){
                resolve(response);
            } else{
                reject(response);
            }
        });
    });
}

Service.prototype.aliasExists = function(alias){
    console.log("checking if alias exists");
    return esClient.indices.existsAlias({name:alias});
}

Service.prototype.createIndex = function(index){
    console.log("creating index");
    return esClient.indices.create({index:index});
}

module.exports = Service;
