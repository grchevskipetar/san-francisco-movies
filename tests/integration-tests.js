var tt = require('blue-tape');
var request = require('request-promise');
var server = require('../routes/routes');



tt.test('Running search service tests', function (t) {
	var app = server(3200);
	var baseUrl = "http://localhost:3200"

	t.test('/init test', function(){
		return request(baseUrl+'/init').then(response => {
			response = JSON.parse(response);
			t.ok(response, 'Got response');
			t.ok(response.done, 'Initialization successful');
		});
	});


	t.test('/reIndex test', function(){
		return request(baseUrl+'/reIndex').then(response => {
			response = JSON.parse(response);
			t.ok(response, 'Got response');
			t.ok(response.done, 'reIndexing successful');
		});
	});

	t.test('/search test', function(){
		return request(baseUrl+'/search/Americana').then(response => {
			response = JSON.parse(response);
			t.ok(response, 'Got response');
			t.ok(response.done, 'Initialization successful');
			t.ok(response.results, 'Should return results');
		});
	});

	t.test('/autocomplete test', function(){
		return request(baseUrl+'/autocomplete/Amer').then(response => {
			response = JSON.parse(response);
			t.ok(response, 'Got response');
			t.ok(response.done, 'Initialization successful');
			t.ok(response.results, 'Should return results');
		});
	});

	t.test('/status test', function(){
		return request(baseUrl+'/status').then(response => {
			response = JSON.parse(response);
			t.ok(response, 'Got response');
			t.ok(response.done, 'Checking status successful');
			t.ok(response.status, 'Status received successfully');
		}).then(() => {
			app.close();
		});
	});

});