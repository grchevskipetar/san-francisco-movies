var tt = require('blue-tape');
var searchService = require('../service/service');
var _ = require('lodash');

tt.test('Running search service tests', function (t) {
	var manager = new searchService();

	var dataForIndexing = [
    	{
    		title: "TEST movie",
            locations: "Some location",
            writer: "Some writer",
            actor_1: "Some actor" ,
            actor_2: "Some other actor" ,
            actor_3: "Some other, other actor" ,
            director: "Some director" ,
            production_company:"Some company",
            release_year:"2016" 
    	},
    	{
    		title: "Some other movie",
            locations: null,
            writer: "Some writer",
            actor_1: "Some actor" ,
            actor_2: "Some other actor" ,
            actor_3: "Some other, other actor" ,
            director: "Some director" ,
            production_company:"Some company",
            release_year:"2016" 
    	},
    	{
    		title: "Some other, other movie",
            locations: "Some location",
            writer: "Some writer",
            actor_1: "Some actor" ,
            actor_2: "Some other actor" ,
            actor_3: "Some other, other actor" ,
            director: "Some director" ,
            production_company:"TEST company",
            release_year:"2016" 
    	}
	];
    var basicSuggest = {
        "type":"completion",
        "analyzer":"simple",
        "payloads":false,
        "preserve_separators":true,
        "preserve_position_increments":true,
        "max_input_length":50   
    }

	var expectedMapping = {
        "movies_1":{
            "mappings":{
                "movie":{
                    "properties":{
                        "actor_1":{"type":"string"},
                        "actor_1Suggest":basicSuggest,
                        "actor_2":{"type":"string"},
                        "actor_2Suggest":basicSuggest,
                        "actor_3":{"type":"string"},
                        "actor_3Suggest":basicSuggest,
                        "director":{"type":"string"},
                        "directorSuggest":basicSuggest,
                        "location":{"type":"string"},
                        "locationSuggest":basicSuggest,
                        "production_company":{"type":"string"},
                        "production_companySuggest":basicSuggest,
                        "release_year":{"type":"string"},
                        "release_yearSuggest":basicSuggest,
                        "title":{"type":"string"},
                        "titleSuggest":basicSuggest,
                        "writer":{"type":"string"},
                        "writerSuggest":basicSuggest
                    }
                }
            }
        }
    }

	var testIndex = "testindex";
	var testAlias = "testalias";


    t.test('createIndex() test', function(){
        return manager.createIndex(testIndex).then(res => {
            t.ok(res, 'Gor response');
            return manager.indexExists(testIndex).then(res => {
                t.ok(res, 'Should find a created index');
            });
        });
    });

    t.test('createAlias() test', function(){
        return manager.createAlias(testAlias,testIndex).then(res => {
            t.ok(res, 'Got response');
            return manager.aliasExists(testAlias).then(res => {
                t.ok(res, 'Should be able to create alias');
            });
        });
    });


    t.test('prepareDataForIndexing() test', function(){
        return manager.prepareDataForIndexing(dataForIndexing, testIndex).then(res => {
            t.ok(res, 'Got response');
            t.equal(4,res.length,'Should have 4 items, 2 actions and 2 action data');
        });
    });

    t.test('bulkIndexItems() test', function(){
        return manager.prepareDataForIndexing(dataForIndexing, testIndex).then(items => {
            return manager.bulkIndexItems(items).then(res => {
                t.ok(res, 'Got response');
                return manager.search("TEST movie", testIndex).then(res => {
                    t.ok(res.hits, 'Should be able to find an item called TEST movie');
                });
            });
        })
    });

    t.test('indexData() test', function(){
        return manager.indexData(dataForIndexing,testIndex).then(res => {
            t.ok(res, 'Got response');
            return manager.search("TEST movie",testIndex).then(res => {
                t.ok(res.hits, 'Should be able to find an item called TEST movie');
            });
        });
    });

    t.test('search() test', function(){ 
        return manager.indexData(dataForIndexing,testIndex).then(res => {
            return manager.search("TEST", testIndex).then(res => {
                t.ok(res, 'Got response');
                var movieResults = _.filter(res.hits, { _source: {title : "TEST movie"}});
                t.notOk(_.isEmpty(movieResults), "Should find movie results for TEST");
                var companyResults = _.filter(res.hits, { _source: {production_company : "TEST company"}});
                t.notOk(_.isEmpty(companyResults), "Should find production company results for TEST");
                return manager.search("fakeItem", testIndex).then(res => {
                    t.notOk(res.total, "Should not find any items");
                });
            });
        });
    });

    t.test('indexExists() test', function(){
        return manager.indexExists(testIndex).then(res => {
            t.ok(res, 'Should be able to find index');
            return manager.indexExists("fakeIndex").then(res => {
                t.notOk(res, 'SHould not be able to find index');
            });
        });
    });

    t.test('aliasExists() test', function(){
        return manager.aliasExists(testAlias).then(res => {
            t.ok(res, 'Should be able to find alias');
            return manager.aliasExists("fakeAlias").then(res => {
                t.notOk(res, "Shouldn't be able to find alias");
            });
        });
    });



	t.test('init() test', function () {
		return manager.init().then(res => {
			t.ok(res,'Got response');
			return manager.indexExists('movies_1').then(res => {
				t.ok(res, "Index named movies_1 should be created");
				return manager.aliasExists('movies').then(res => {
					t.ok(res, "Alias named movies should be created");
					return manager.search('American', 'movies').then(res => {
						t.ok(res.total>0, "Should find items that contain the word American");
					});
				});
			});
		});
    });

    t.test('initMapping() test', function(){
    	return manager.initMapping().then(res => {
    		t.ok(res, 'Got response');
    		return manager.getMapping("movies").then(res => {
    			t.ok(_.isEqual(res,expectedMapping), 'Should be equal to expectedMapping');
    		});
    	});
    });

    t.test('getMapping() test', function() {
    	return manager.getMapping('movies').then(res => {
    		t.ok(res, 'Got response');
    		t.ok(_.isEqual(res,expectedMapping), 'Should be equal to expectedMapping');
    	});
    });

    t.test('reIndex() test', function() {
    	return manager.reIndex().then(res => {
    		t.ok(res, 'Got response');
    		return manager.indexExists('movies_1').then(res => {
    			t.notOk(res, "Should not find an index with name movies_1");
				return manager.indexExists('movies_2').then(res => {
					t.ok(res, "Should find an index with name movies_2");
					return manager.aliasExists('movies').then(res => {
						t.ok(res, "Alias named movies should be created");
						return manager.search('American', 'movies').then(res => {
							t.ok(res.total>0, "Should find items that contain the word American");
						});
					});
				});
    		});
    	});
    });

    t.test('getDataForImport() test', function(){
    	return manager.getDataForImport().then(res => {
    		t.ok(res, 'Got response');
    		t.ok(res.length, 'Should be able to get items for import');
    	});
    });

    //TODO make general
    t.test('autocompleteSuggestions() test', function(){
    	return manager.autocompleteSuggestions('Amer').then(res => {
    		t.ok(res, 'Got response');
    		t.equals(res[0].text, 'Americana', 'Should suggest Americana');
            return manager.autocompleteSuggestions('fakeSuggestionText').then(res => {
                t.notOk(res.length, 'Should not suggest anything');
            });
    	});
    });

    t.test('autocompleteSuggestions() and search() compatibility test', function(){
        return manager.autocompleteSuggestions('Amer').then(res => {
            return manager.search(res[0].text).then(results => {
                t.notOk(_.isEmpty(results), 'Should find results for term suggested by autcompletion feature');
            });
        });
    });

    t.test('status() test', function(){
        return manager.status().then(res => {
            t.ok(res, 'Got response');
            t.equals(res.service, "OK", 'Service status should be okay');
            t.equals(res.search, "OK", 'Search status should be okay');
            t.equals(res.autocomplete, "OK", 'Autocomplete status should be okay');
        });
    });
});
