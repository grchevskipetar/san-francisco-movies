var _ = require('lodash');
var fs = require('fs');
var all = './logs.txt';
var errors = './errors.txt';


var loggerType = "";

function Logger(type) {
	if(type)
		loggerType = type
	else
		loggerType = "info"
}

Logger.prototype.log = function(message,err) {
	var toWrite = {
		timestamp: Math.floor(Date.now() / 1000),
		message:message,
		error:err
	}
	var toWriteLine = JSON.stringify(toWrite) +'\n';

	if(loggerType === "info"){
		fs.appendFile(all, toWriteLine);
	}

	if(err){
		fs.appendFile(errors, toWriteLine);
	}
}

Logger.prototype.cleanLogs = function(){
	fs.truncate(all, 0);
	fs.truncate(errors, 0);
}


module.exports = Logger;


