SF Movies project
=================

The problem
-----------

The goal of the project was to provide a service which would allow users to perform search, boosted with an autocomplete feature, of films that have been filmed in San Francisco, and have their locations shown on a map.

The solution
------------

I took the back-end route to solving the problem and developed a service which would provide search and autocomplete features. Using the data available [here](https://data.sfgov.org/Arts-Culture-and-Recreation-/Film-Locations-in-San-Francisco/yitu-d5am), the service allows you to perform searches on all of the fields as well as to get autocomplete suggestions. The results can then be used by the front-end to have them drawn on a map using Google Maps or similar APIs.

Technical choices
-----------------

**Technology:** 
I decided to use Node.js to develop the service. I picked Node.js because I have the most experience with it in developing web services. I've worked with Node.js professionally for 6 months. Most of the libaries used are common among Javascript developers. One possible special choice would be using 'blue-tape' for testing. This is due to my familiarity with it from work.

**Architecture:**
The main dilemma I had was whether I should host the data myself or use the API provided with the source of the data. I decided to host the data myself because I wanted to have full control over my service's availability, because should the source go down my service would become unavailable. This way, I would be responsible for my uptime and not depend on anyone else.

After doing some research I decided to use Elasticsearch. I think that a search engine the likes of Elasticsearch is a better fit for the needs of the service than a standard SQL or NoSQL database. Why I chose Elasticsearch as opposed so Solr for example was due to its integration with AWS and previous experience I've had with Solr.

Both of the Elasticsearch server and the Node.js server are hosted on Amazon AWS.

API
---
### GET /
Loads the HTML page or the front-end of the application.


### GET /init
Performs an initialization of the service. The initialization involves deleting any pre-existing indices in the Elasticsearch cluster, creating an index, an alias and finally populating the index with the San Fracisco movie data.

Returns `{done: true, message: "Done initializing"}`

### GET /reIndex
Performs a reIndexing of the service. The reIndexing involves creating a new index and assigning the alias to also cover the index, populating the new index and afterwards deleting the old index.

Returns `{done: true, message: "Done reindexing"}`

### GET /search/:searchTerm
Performs a search.

Path variable:
- **searchTerm** - the term to perform searh for.

Returns `{done: true, results: [Object] }`

### GET /autocomplete/:inputText
Gets autocomplete suggestions for the intput text. Takes misspelling into account.

Path variable:
- **inputText* - the text to get suggestinos for.

Returns `{done: true, results: [Object] }`

### GET /status
Gets the status of the service or specifically the search and autocomplete functionalities.

Returns `{done: true, status: { service: 'OK' || 'UNHEALTHY', search: 'OK' || 'UNHEALTHY', autocomplete: 'OK' || 'UNHEALTHY'} }`

Security
--------

There are two points of interest when it comes to security, the Elasticsearch server and the Node.js server or the application.

The Elasticsearch server is currently publicly accessible through its url. Why I made it publicly accessible is because I didn't have time to setup a new server that would be used for testing. Therefore, in order to run tests locally, I made the server have public access. By simply setting the policy in the AWS configuratino of the server, to only accept requests from the ip of the instance where the application is deployed, this security flaw would be taken care of.

Some of the API functions, namely /init and /reIndex should not be publicly accessible. This can be done through the nginx running on the instance. We can require authorization for accessing these functions, whereas the /search and /autocompelte functions can remain accessible. 

Testing
-------

I have included unit and integration tests within the tests directory of the project. The tests have been done using 'blue-tape'. Simply run node tests/unit-tests.js or node tests/integration-tests.js to execute them. The tests do not provide full coverage, since they don't take all cases into account and some of them are not general, but specific to the dataset itself. This can be improved.

Logging
-------

I made a simple logger which would log a message whenever a command is used. Didn't manage to fully develop it so it is in a prototype state.

Monitoring
----------

Some basic monitoring is available through the /status api call and Amazon AWS console.

Points of improvement and technical tradeoffs
---------------------------------------------

It is very important to note that many of the choices that I made were influenced by the dataset itself, namely its relatively small size. The Elasticsearch service provides searching and autocompletion for all fields of the dataset. Should the size of the dataset increase significantly, there might be a need to restrict autocompletion to work only on titles in order to be able to provide fast responses.

Inconsistensy in the dataset. From what I could figure out about the dataset itself, is that the locations field was inconsistent. At first it seemed like the different locations are separated by commas, but then it turned out commas were also used for specifying an alias for the location or an area. Sometime, the locations were separated by semi-colons or & symbols, making it difficult to distinguish whether it is a single location in question or whether it's multiple locations. Because of this, some of the locations might be ommited. If I were to spend additional time on the project I might be able to figure out how to decide whether something forms a single location or whether it is multiple locations in question.

There were some entries in the dataset which didn't have a location specified and have been purposely ommitted from the results.

I had the intention to aslo add a failover mechanism where the search query is not send to Elasticsearch, but to the API provided by the source of the dataset. This would result in degradation of service due to absence of an autocompletion, but would increase availability.

The security improvements I could make have already been mentioned under **Security**.

The test improvements I could make have already been mentioned under **Testing**.

Hosted app link
---------------

http://sanfranciscomoviesapp-env.us-west-1.elasticbeanstalk.com/

LinkedIn profile
----------------

https://www.linkedin.com/in/petar-grchevski-a3154167?trk=hp-identity-photo