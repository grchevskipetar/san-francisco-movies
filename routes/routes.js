var express = require('express');
var request = require('request');
var fs = require('fs');
var app = express();
var elasticSearch = require('elasticsearch');
var config = require('../config/config');
var searchService = require('../service/service');
var manager = new searchService();

var createServer = function(port){

	app.get('/', function (req, res) {
		res.sendFile(__dirname + '/index.html');
	});

	app.get('/status', function(req,res){
		manager.status().then(response => {
			res.json({done:true, status:response});
		});
	});

	app.get('/reIndex', function(req,res){
		manager.reIndex().then(response => {
			res.json({done:true, message:"Done reindexing"});
		});
	});

	app.get('/init', function(req,res){
		manager.init().then(response => {
			res.json({done:true, message:"Done initializing"});
		});
	});

	app.get('/search/:searchTerm', function(req,res){
		manager.search(req.params.searchTerm, config.alias).then(results => {
			res.json({done:true, results:results});
		});
	});

	app.get('/autocomplete/:inputText', function(req,res){
		manager.autocompleteSuggestions(req.params.inputText).then(results => {
			res.json({done:true, results:results});
		});
	});

	return app.listen(port, function () {
		console.log('San Francisco search running on port '+port);
	});
}

module.exports = createServer;



